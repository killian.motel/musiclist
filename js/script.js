

function post(url, data) {
    return new Promise((ok, err) => {
        let xhr=new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(data));
        xhr.onload=() => {
            if(xhr.status/100!=2) err(xhr);
            else ok(xhr.responseText);
        };
        xhr.onerror=() => err(xhr);
    });
}
function get(url, json) {
    return new Promise((ok, err) => {
        let xhr=new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.send(null);
        xhr.onload=() => {
            if(xhr.status/100!=2) err(xhr);
            else ok((json)?(JSON.parse(xhr.responseText)):(xhr.responseText));
        }
        xhr.onerror=() => err(xhr);
    });
}

const Musique = {
    data: function() {
        return {
            errors: [],
            totalList: {},
            musicList: {
                name: "",
                musics: []
            },
            selectedIndex: -1,
            currentList: '',
            id: 0,
            titre: '',
            groupe: '',
            album: '',
            date: '',
            listName: '',
            selected: ''
        }   
    },

    template: `
        <div class="container text-center ">
            <h1 class="display-3 mt-5" align="center"> My Music List </h1>
            <div class="row">
                <div v-if="currentList" class="col-lg-8">
                    <div class="card mb-2 nomListe col">

                            <button type="button" class="close mr-5" v-on:click="deleteList"></button>
                            <button type="button" class="editable mr-4" v-on:click="editList"></button>
                        
                        <span class="list-name col"> <h1>{{totalList[currentList].name}} </h1></span>     
                    </div>
                    <div class="list-group corps mb-2 bg-light col-sm-12 mr-5" >
                        <p href="#" class="list-group-item" v-for="m in totalList[currentList].musics">
                            <button type="button" class="close" :data-id="m.id" v-on:click="deleteSong"></button>
                            <span class="song-title">{{ m.titre }}</span>
                            (<span class="song-date">{{ m.date }}</span>) </br>
                            artist(s) : <span class="song-group">{{ m.groupe }}</span> -
                            album : <span class="song-album">{{ m.album }}</span>
                            <button type="button" class="editable mr-2" :data-id="m.id" v-on:click="editSong"></button>
                            <br>
                        </p>
                    </div>
                    
                </div>
                <div v-else class="col-lg-8">
                    <h1> Veuillez créer une liste </h1>
                </div>


                <div class="col-lg-4">
                    <div class="card border-primary mb-3 row" style="max-width: 20rem;">
                        <div class="card-header">Ajouter/Sélectionner une liste</div>
                        <div class="card-body">
                            <p class="card-text">
                                <div class="row">
                                    <input class="form-control col-sm-8" type="text" placeholder="Nom liste" v-model="listName" />
                                    <button type="button" class="btn btn-primary col-sm-4" v-on:click="addList">Add</button>
                                </div>
                                <div class="row mt-2">
                                    <select v-model="selected" @change="listSelect($event)" class="form-control col-sm" id="exampleSelect1">
                                        <option disabled value=""> Sélectionner une liste </option>
                                        <option v-for="option in totalList" :value="option" class="partOfList">
                                            {{ option.name }} 
                                        </option>
                                    </select>
                                </div>
                            </p>
                        </div>
                    </div>
                        
                    <div class="send card border-secondary mb-3 mt-2 align-center row" style="max-width: 20rem;" >
                        <div class="card-header"> Ajouter un morceau </div>
                        <div class="card-body">
                            <p class="card-text">
                                <div class="row mb-2">
                                    <div v-if="errors[0] == 'title'" class="col-sm-8 ">
                                        <input class="form-control is-invalid" type="text" placeholder="Titre*" v-model="titre" />
                                    </div>
                                    <div v-else class="col-sm-8 ">
                                        <input class="form-control" type="text" placeholder="Titre*" v-model="titre" />
                                    </div>
                                    <input class="col-sm-4" type="text" placeholder="Date" v-model="date" />
                                </div>
                                <div class="row">
                                    <input class="col-sm-6" type="text" placeholder="Album" v-model="album" />
                                    <div v-if="errors[1] != 'group'" class="col-sm-6">
                                        <input class="form-control" type="text" placeholder="Groupe*" v-model="groupe" />
                                    </div>
                                    <div v-else class="col-sm-6">
                                        <input class="form-control is-invalid" type="text" placeholder="Groupe*" v-model="groupe" />
                                    </div>
                                    <br>
                                </div>
                                
                                <div class="row">
                                    <input value="Ajouter" class="col-sm mt-2" type="submit" v-on:click="addSong" />
                                </div>
                            </p>
                        </div>
                    </div>
                            
                    <h3 class="mt-5"> Ou importer son JSON </h3> 
                    <div class="form-group row">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input  type="file" class="custom-file-input" id="inputGroupFile" v-on:input="importJSON">
                                <label class="custom-file-label" for="inputGroupFile"></label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                    </div>
                    
                    <h3 class="mt-5 row"> Ou exporter le JSON </h3> 
                    <input type="submit" value="Générer le JSON de la liste" v-on:click="generateJSON" class="mb-3 row" />

                </div>
            </div>
        </div>

    `,

    methods: {
        checkForm: function(){
            this.errors = [];

            if(!this.titre) this.errors.push('title');
            if(!this.groupe) this.errors.push('group');
            if(this.errors.length) return true;
            
            return false;
        },
        getLists: async function() {
            this.totalList = await get("php/get.php", true); // fetch pose problème avec le cache
        },
        getSongs: async function() {
            if(!this.getFirstList()) return 0;

            this.totalList[this.currentList].musics.map((m, id) => {
                const { titre, groupe, album, date } = m;
                return {
                    titre,
                    groupe,
                    album,
                    date,
                    id
                };
            });
        },
        addSong: async function() {
            if (!this.currentList) return 0;

            const { titre, groupe, album, date } = this;

            if(this.checkForm()){
                this.groupe = "";
                this.titre = "";
                this.album = "";
                this.date = "";
                return 0;
            }
        
            this.totalList[this.currentList].musics.push({
                groupe: groupe,
                titre: titre,
                album: album || '?',
                date: date || '?',
                id: this.totalList[this.currentList].musics.map(a => a.id).reduce(((a, b) => a>b?a:b), 0)+1
            });

            this.groupe = "";
            this.titre = "";
            this.album = "";
            this.date = "";

            await this.getSongs();
            await this.majJSON();
        },
        deleteList: async function(e){
            const idx = this.totalList[this.currentList];
            delete this.totalList[this.currentList];
            
            this.currentList = this.getFirstList();

            await this.majJSON();
        },
        deleteSong: async function(e){
            const id = e.target.getAttribute("data-id");
            const idx = this.totalList[this.currentList].musics.findIndex(m => m.id==id);
            if(idx!=-1) this.totalList[this.currentList].musics.splice(idx, 1);

            await this.majJSON();
        },
        editList: async function(evt){
            const btn = evt.target;
            const p = btn.parentElement;
            const editing = !btn.classList.contains("editing");
            btn.classList.toggle("editing");
            if(editing) {
                p.querySelectorAll("span").forEach(e => e.contentEditable = true);
            } else {
                const m = this.totalList[this.currentList];
                delete this.totalList[this.currentList];
                m.name = p.querySelector('.list-name').innerText;
                this.totalList[m.name] = m;
                this.currentList = m.name;
                p.querySelectorAll("span").forEach(e => e.removeAttribute('contenteditable'));

                await this.majJSON();
            }
        },
        editSong:async function(evt){
            const btn = evt.target;
            const p = btn.parentElement;
            const editing = !btn.classList.contains("editing");
            btn.classList.toggle("editing");
            if(editing) {
                p.querySelectorAll("span").forEach(e => e.contentEditable = true);
            } else {
                const id = evt.target.getAttribute("data-id");
                const m = this.totalList[this.currentList].musics.find(m => m.id==id);
                m.titre = p.querySelector('.song-title').innerText;
                m.date = p.querySelector('.song-date').innerText;
                m.album = p.querySelector('.song-album').innerText;
                m.groupe = p.querySelector('.song-group').innerText;
                p.querySelectorAll("span").forEach(e => e.removeAttribute('contenteditable'));

                await this.majJSON();
            }
        },
        generateJSON: function() {
            const filename = 'My_music_list.json';
            const data = JSON.stringify(this.totalList);
            let element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));  
            element.setAttribute('download', filename);
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
        },
        importJSON: async function(){
            const fileSelector = document.getElementById("inputGroupFile");
            
            var fullPath = document.getElementById('inputGroupFile').value;
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    var nameToChange = document.getElementsByClassName("custom-file-label")[0];
                    nameToChange.innerText = filename.substring(1);
                }
            }
            try {
                const file = fileSelector.files[0];
                const data = JSON.parse(await file.text());
                
                this.totalList = data;
                this.currentList = this.getFirstList();
                await this.majJSON();
            } catch(e) {
                console.error(e);
            }
        },
        majJSON: async function(){
            await post('php/update.php', this.totalList);
            const data = JSON.stringify(this.totalList);
            let element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
            await this.getSongs();
        },
        addList: async function(){
            const listName = this;
            if(!this.listName) return 0;
            if(this.totalList.length != 0){ 
                for(let i in this.totalList){
                    if(i == this.listName) return 0;
                }
            }
      
            this.totalList[this.listName] = {
                name: this.listName,
                musics: []
            };
             
            this.currentList = this.listName;
            this.selected = this.currentList;
            this.listName = "";
        
            await this.majJSON();
        },
        getFirstList: function(){
            for(list in this.totalList) return list;
        },
        listSelect(event){
           this.currentList = this.selected.name;
        }
    },
    async created() {
        await this.getLists();
        this.currentList = this.getFirstList();
      }
};



const Menu = {
    data: function() {
        return {
            
        }
    },

    template: `
        <div class="jumbotron">
            <h1 class="display-3">Créer sa liste ?!</h1>
            <p class="lead">
                C'est simple ! </br>
                Il te suffit d'aller dans "Morceaux" et d'ajouter tes morceaux préférés après avoir créer ta liste,  ! (Seuls "Titre" et "Groupe" sont requis)</br>
                Tu peux aussi télécharger ta liste au format JSON afin de l'importer sur une autre machine ;) </br>
                Et à l'inverse, tu peux donc importer une liste au format JSON ! </br>
                /!\\ Il faut que les objets de la liste aient bien les attributs "titre", "date", "album", "groupe" et "id" /!\\ <br>
                <br>
                Tu peux aussi cliquer sur le "X" afin de supprimer le morceau correspondant de la liste, <br>
                ou bien l'éditer en cliquant une première fois sur "Edit" et en réappuyant pour valider les changements <br>
                et pareil pour les listes.
            </p>
            <hr class="my-4">
            <p>
                Clique sur le bouton en dessous afin d'accéder à ta liste
            </p>
            <p class="lead">
                <router-link to="/musique" class="btn btn-primary btn-lg">Ma liste de musiques</router-link>
            </p>
        </div>
    `
};

const routes = [
    { path: '/musique', component: Musique },
    { path: '/', component: Menu }

];

const router = new VueRouter({
    routes: routes
});

new Vue({
    router

}).$mount('#app');